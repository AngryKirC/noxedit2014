﻿#region Using directives

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using NoxShared;
using static NoxShared.Map;


#endregion

namespace MapEditor
{
    partial class GroupDialog : Form
    {
        public Map.GroupData GroupD { get; set; }
		protected Map.Group selected;
		protected Map.Group.GroupTypes newType;

        public GroupDialog()
        {
            InitializeComponent();
            groupsList.DisplayMember = "name";
            groupsList.FormattingEnabled = false;
        }

		public void RefreshGroupsList()
		{
			groupsList.Items.Clear();
            foreach (Map.Group g in GroupD.Groups)
                groupsList.Items.Add(g);
        }

        protected void UpdateGroupItems(Group group)
        {
            group.Clear();
            switch (group.type)
            {
                case Map.Group.GroupTypes.objects:
                    foreach (String s in itemList.Text.Trim().Split('\n'))
                        group.Add(Int32.Parse(s.Trim()));
                    break;
                case Map.Group.GroupTypes.walls:
                    foreach (String s in itemList.Text.Split('\n'))
                    {
                        string[] loc = s.Trim().Split(',');
                        if (loc.Length != 2)
                            break;
                        group.Add(new Point(Int32.Parse(loc[0]), Int32.Parse(loc[1])));
                    }
                    break;
                case Map.Group.GroupTypes.waypoint:
                    foreach (String s in itemList.Text.Split('\n'))
                        group.Add(Int32.Parse(s.Trim()));
                    break;
                default:
                    break;
            }
        }

        private void updateButton_Click(object sender, EventArgs e)
        {
			if (selected != null)
			{
				selected.type = newType;
                selected.name = groupNameTextBox.Text;
                UpdateGroupItems(selected);

                RefreshGroupsList();
                groupsList.SelectedItem = selected;
			}
        }

        private void objectRadio_CheckedChanged(object sender, EventArgs e)
        {
            if (objectRadio.Checked)
            {
                wallRadio.Checked = false;
                waypointRadio.Checked = false;
                newType = Map.Group.GroupTypes.objects;
            }
        }

        private void wallRadio_CheckedChanged(object sender, EventArgs e)
        {
            if (wallRadio.Checked)
            {
                objectRadio.Checked = false;
                waypointRadio.Checked = false;
                newType = Map.Group.GroupTypes.walls;
            }
        }

        private void closeButton_Click(object sender, EventArgs e)
        {
            Close();
        }

        private void waypointRadio_CheckedChanged(object sender, EventArgs e)
        {
            if (waypointRadio.Checked)
            {
                objectRadio.Checked = false;
                wallRadio.Checked = false;
                newType = Map.Group.GroupTypes.waypoint;
            }
        }

		private void delButton_Click(object sender, EventArgs e)
		{
			if (selected != null && groupsList.SelectedIndex >= 0)
			{
                int index = groupsList.SelectedIndex - 1;
				GroupD.Groups.Remove(selected);

                RefreshGroupsList();
                groupsList.SelectedIndex = index;
			}
		}

		private void createButton_Click(object sender, EventArgs e)
		{
            if (groupNameTextBox.Text.Length <= 0)
                return;

            Group result = new Group(groupNameTextBox.Text, newType, -1);
            UpdateGroupItems(result);
            for (int i = 0; i < 65535; i++)
            {
                bool found = false;
                foreach (Group gg in GroupD.Groups)
                {
                    if (gg.id == i)
                    {
                        found = true;
                        break;
                    }
                }
                if (!found)
                {
                    result.id = i;
                    break;
                }
            }
            GroupD.Groups.Add(result);

            RefreshGroupsList();
            groupsList.SelectedItem = result;
		}

		private void groupsList_SelectedIndexChanged(object sender, EventArgs e)
		{
            if (groupsList.SelectedIndex >= 0)
            {
                selected = (Map.Group)groupsList.SelectedItem;
                groupNameTextBox.Text = selected.name;

                itemList.Clear();
                switch (selected.type)
                {
                    case Map.Group.GroupTypes.objects:
                        foreach (Int32 i in selected)
                            itemList.Text += String.Format("{0}\r\n", i);
                        wallRadio.Checked = false;
                        objectRadio.Checked = true;
                        waypointRadio.Checked = false;
                        break;
                    case Map.Group.GroupTypes.waypoint:
                        foreach (Int32 i in selected)
                            itemList.Text += String.Format("{0}\r\n", i);
                        wallRadio.Checked = false;
                        objectRadio.Checked = false;
                        waypointRadio.Checked = true;
                        break;
                    case Map.Group.GroupTypes.walls:
                        foreach (Point pt in selected)
                            itemList.Text += String.Format("{0},{1}\r\n", pt.X, pt.Y);
                        wallRadio.Checked = true;
                        objectRadio.Checked = false;
                        waypointRadio.Checked = false;
                        break;
                }
            }
            else
                selected = null;
        }
	}
}