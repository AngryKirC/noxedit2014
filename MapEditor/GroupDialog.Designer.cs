﻿namespace MapEditor
{
    partial class GroupDialog
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.updateButton = new System.Windows.Forms.Button();
            this.groupBoxTypes = new System.Windows.Forms.GroupBox();
            this.waypointRadio = new System.Windows.Forms.RadioButton();
            this.wallRadio = new System.Windows.Forms.RadioButton();
            this.objectRadio = new System.Windows.Forms.RadioButton();
            this.closeButton = new System.Windows.Forms.Button();
            this.delButton = new System.Windows.Forms.Button();
            this.itemList = new System.Windows.Forms.TextBox();
            this.createButton = new System.Windows.Forms.Button();
            this.groupNameTextBox = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.groupsList = new System.Windows.Forms.ListBox();
            this.label2 = new System.Windows.Forms.Label();
            this.groupBoxTypes.SuspendLayout();
            this.SuspendLayout();
            // 
            // updateButton
            // 
            this.updateButton.Location = new System.Drawing.Point(195, 169);
            this.updateButton.Name = "updateButton";
            this.updateButton.Size = new System.Drawing.Size(75, 23);
            this.updateButton.TabIndex = 2;
            this.updateButton.Text = "Update";
            this.updateButton.Click += new System.EventHandler(this.updateButton_Click);
            // 
            // groupBoxTypes
            // 
            this.groupBoxTypes.Controls.Add(this.waypointRadio);
            this.groupBoxTypes.Controls.Add(this.wallRadio);
            this.groupBoxTypes.Controls.Add(this.objectRadio);
            this.groupBoxTypes.Location = new System.Drawing.Point(195, 12);
            this.groupBoxTypes.Name = "groupBoxTypes";
            this.groupBoxTypes.Size = new System.Drawing.Size(78, 88);
            this.groupBoxTypes.TabIndex = 3;
            this.groupBoxTypes.TabStop = false;
            this.groupBoxTypes.Text = "Type";
            // 
            // waypointRadio
            // 
            this.waypointRadio.AutoSize = true;
            this.waypointRadio.Location = new System.Drawing.Point(7, 64);
            this.waypointRadio.Margin = new System.Windows.Forms.Padding(3, 1, 3, 3);
            this.waypointRadio.Name = "waypointRadio";
            this.waypointRadio.Size = new System.Drawing.Size(70, 17);
            this.waypointRadio.TabIndex = 2;
            this.waypointRadio.Text = "Waypoint";
            this.waypointRadio.CheckedChanged += new System.EventHandler(this.waypointRadio_CheckedChanged);
            // 
            // wallRadio
            // 
            this.wallRadio.AutoSize = true;
            this.wallRadio.Location = new System.Drawing.Point(7, 43);
            this.wallRadio.Margin = new System.Windows.Forms.Padding(3, 3, 3, 2);
            this.wallRadio.Name = "wallRadio";
            this.wallRadio.Size = new System.Drawing.Size(51, 17);
            this.wallRadio.TabIndex = 1;
            this.wallRadio.Text = "Walls";
            this.wallRadio.CheckedChanged += new System.EventHandler(this.wallRadio_CheckedChanged);
            // 
            // objectRadio
            // 
            this.objectRadio.AutoSize = true;
            this.objectRadio.Location = new System.Drawing.Point(7, 20);
            this.objectRadio.Name = "objectRadio";
            this.objectRadio.Size = new System.Drawing.Size(61, 17);
            this.objectRadio.TabIndex = 0;
            this.objectRadio.Text = "Objects";
            this.objectRadio.CheckedChanged += new System.EventHandler(this.objectRadio_CheckedChanged);
            // 
            // closeButton
            // 
            this.closeButton.Location = new System.Drawing.Point(195, 198);
            this.closeButton.Name = "closeButton";
            this.closeButton.Size = new System.Drawing.Size(75, 23);
            this.closeButton.TabIndex = 0;
            this.closeButton.Text = "Close";
            this.closeButton.Click += new System.EventHandler(this.closeButton_Click);
            // 
            // delButton
            // 
            this.delButton.Location = new System.Drawing.Point(195, 140);
            this.delButton.Name = "delButton";
            this.delButton.Size = new System.Drawing.Size(75, 23);
            this.delButton.TabIndex = 6;
            this.delButton.Text = "Delete";
            this.delButton.Click += new System.EventHandler(this.delButton_Click);
            // 
            // itemList
            // 
            this.itemList.Location = new System.Drawing.Point(282, 35);
            this.itemList.Multiline = true;
            this.itemList.Name = "itemList";
            this.itemList.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.itemList.Size = new System.Drawing.Size(189, 186);
            this.itemList.TabIndex = 7;
            // 
            // createButton
            // 
            this.createButton.Location = new System.Drawing.Point(195, 111);
            this.createButton.Name = "createButton";
            this.createButton.Size = new System.Drawing.Size(75, 23);
            this.createButton.TabIndex = 8;
            this.createButton.Text = "Create";
            this.createButton.UseVisualStyleBackColor = true;
            this.createButton.Click += new System.EventHandler(this.createButton_Click);
            // 
            // groupNameTextBox
            // 
            this.groupNameTextBox.Location = new System.Drawing.Point(320, 9);
            this.groupNameTextBox.Name = "groupNameTextBox";
            this.groupNameTextBox.Size = new System.Drawing.Size(151, 20);
            this.groupNameTextBox.TabIndex = 9;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(279, 12);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(35, 13);
            this.label1.TabIndex = 10;
            this.label1.Text = "Name";
            // 
            // groupsList
            // 
            this.groupsList.FormattingEnabled = true;
            this.groupsList.Location = new System.Drawing.Point(12, 35);
            this.groupsList.Name = "groupsList";
            this.groupsList.Size = new System.Drawing.Size(168, 186);
            this.groupsList.TabIndex = 11;
            this.groupsList.SelectedIndexChanged += new System.EventHandler(this.groupsList_SelectedIndexChanged);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(12, 12);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(70, 13);
            this.label2.TabIndex = 12;
            this.label2.Text = "List of groups";
            // 
            // GroupDialog
            // 
            this.AutoScaleBaseSize = new System.Drawing.Size(5, 13);
            this.ClientSize = new System.Drawing.Size(484, 240);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.groupsList);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.groupNameTextBox);
            this.Controls.Add(this.createButton);
            this.Controls.Add(this.itemList);
            this.Controls.Add(this.delButton);
            this.Controls.Add(this.closeButton);
            this.Controls.Add(this.groupBoxTypes);
            this.Controls.Add(this.updateButton);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
            this.MaximizeBox = false;
            this.Name = "GroupDialog";
            this.SizeGripStyle = System.Windows.Forms.SizeGripStyle.Hide;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "Groups Editor";
            this.groupBoxTypes.ResumeLayout(false);
            this.groupBoxTypes.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.Button updateButton;
        private System.Windows.Forms.GroupBox groupBoxTypes;
        private System.Windows.Forms.RadioButton objectRadio;
		private System.Windows.Forms.RadioButton wallRadio;
        private System.Windows.Forms.Button closeButton;
        private System.Windows.Forms.RadioButton waypointRadio;
		private System.Windows.Forms.Button delButton;
		private System.Windows.Forms.TextBox itemList;
        private System.Windows.Forms.Button createButton;
        private System.Windows.Forms.TextBox groupNameTextBox;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.ListBox groupsList;
        private System.Windows.Forms.Label label2;
    }
}